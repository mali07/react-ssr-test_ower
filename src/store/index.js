import {
    createStore,
    applyMiddleware
} from 'redux';
import thunk from 'redux-thunk';
import {
    composeWithDevTools
} from 'redux-devtools-extension'
import reducers from './reducers'
import serverRequst from '../server/request'
import clientRequst from '../client/request'


export function getClientStore() {
    let initState = window.content.state;
    return createStore(reducers, initState, composeWithDevTools(
        applyMiddleware(thunk.withExtraArgument(clientRequst))
    ));
}

export function getServerStore(req) { 
    return createStore(reducers, composeWithDevTools(
        applyMiddleware(thunk.withExtraArgument(serverRequst(req)))
    ));
}
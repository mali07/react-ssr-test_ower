import *as types from '../action_types'
import axios from 'axios'
export default {
    add() { 
        return {type:types.ADD}
    },
    getLikes() {
         let url = '/api/info/app/getYouMayLike/1/1?pageSize=5';
        return (dispatch,getState,request) => {
            return request.get(url).then(res => {
                res = res.data;
                if (res.status == 0) {
                    dispatch({
                        type: types.SETLIKES,
                        data: res.result
                    })
                }
            })
        }
    }
}
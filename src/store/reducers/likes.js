import * as types from '../action_types'
let initState = {
    list: []
}
export default function reducer(state = initState, action) {
    switch (action.type) {
        case types.SETLIKES:
            return {
                ...state,
                list: action.data
            }
        default:
            return state
    }
}
import { combineReducers } from 'redux'
import counter from '../reducers/counter'
import likes from '../reducers/likes'
let reducers = combineReducers({
    counter,
    likes
})
export default reducers
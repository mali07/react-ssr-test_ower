import * as types from '../action_types'
let initState = {
    count: 0
}
export default function reducer(state = initState, action) {
    switch (action.type) {
        case types.ADD:
            return {
                count: state.count + 1
            }
            default:
                return state
    }
}
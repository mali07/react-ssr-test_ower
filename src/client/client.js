import React from 'react'
import ReactDOM from 'react-dom'
import { BrowserRouter, Route } from 'react-router-dom'
import { renderRoutes} from 'react-router-config'
import routes from '../routes'
import Header from '../compontents/Header'
import { Provider } from 'react-redux'
import { getClientStore} from '../store'
ReactDOM.hydrate(
    <Provider store={getClientStore()}>
        <BrowserRouter>
            <div className='container'>
                <Header/>
                {renderRoutes(routes)}
            </div>
        </BrowserRouter>
    </Provider>
    , document.getElementById('root'))
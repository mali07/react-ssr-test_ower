import fetchJsonp from 'fetch-jsonp'
export default function ajax(url) {
    return new Promise((resolve, reject) => {
        fetchJsonp(url).then(res => {
            return res.json()
        }).then(res => {
            resolve(res)
        }).catch(err => {
            console.log('parsing failed' + url)
            reject(err);
        })
    })
}
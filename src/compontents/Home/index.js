import React, { Component } from 'react'
import styles from './style.css'
import widthStyle from '../withStyle'
 class Home extends Component{
    componentWillMount() { 
        if (this.props.staticContext) { 
            this.props.staticContext.csses.push(styles._getCss())
        }
    }
    render() { 
        return (
            <div className={styles.home}>
                home
            </div>
        )
    }
}
export default widthStyle(Home,styles)
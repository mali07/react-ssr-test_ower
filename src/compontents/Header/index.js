import React from 'react'
import { Link } from 'react-router-dom'
export default class Header extends React.Component { 
    render() { 
        return (
            <ul>
                <li>
                    <Link to='/'>列表页</Link>
                </li>
                <li>
                    <Link to='/counter'>详情页</Link>
                </li>
                <li>
                    <Link to='/likes'>猜你喜欢</Link>
                </li>
                <li>
                    <Link to='/person'>个人中心</Link>
                </li>
            </ul>
        )
    }
}
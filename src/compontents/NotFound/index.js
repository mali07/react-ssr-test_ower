import React from 'react'
export default class NotFound extends React.Component { 
    componentWillMount() { 
        if (this.props.staticContext) { 
            this.props.staticContext.noFound=true
        }
    }
    render() { 
        return (
            <div>你的页面找不到了...</div>
        )
    }
}
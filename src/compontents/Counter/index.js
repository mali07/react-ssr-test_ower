import React, { Component } from 'react'
import { connect } from 'react-redux'
import actions from '../../store/actions'
 class Counter extends Component {
     render() { 
        return (
            <div>
                {this.props.count}
                <button onClick={this.props.add}>+</button>
            </div>
        )
    }
}
export default connect(state => state.counter, actions)(Counter)
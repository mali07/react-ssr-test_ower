import React, { Component } from 'react'
import { Redirect } from 'react-router-dom'
export default class Person extends Component { 
    render() { 
        let { user } = this.props;
        return (
            <div>
                {
                    user ? 
                        <div>666</div>
                        :
                        <Redirect to='/likes'/>
                }
            </div>
        )
    }
}
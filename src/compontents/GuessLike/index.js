import React from 'react'
import { connect } from 'react-redux'
import actions from '../../store/actions'
import styles from './style.css'
import withStyle from '../withStyle'
class GuessLike extends React.Component { 
    componentDidMount() {
        if (this.props.list.length == 0) { 
            this.props.getLikes()
        } 
    }
    render() { 
        let { list} =this.props
        return (
            <div>
                <ul className={styles.likes}>
                    {
                        list.map((item,index)=>{
                        return <li key={index}>{item.infoid}</li>
                    })
                    }
                </ul>
            </div>
        )
    }
}

GuessLike = withStyle(GuessLike, styles);
GuessLike = connect(state => state.likes, actions)(GuessLike);
//服务端请求数据
GuessLike.loadData = function (store) {
    return store.dispatch(actions.getLikes())
}

export default GuessLike
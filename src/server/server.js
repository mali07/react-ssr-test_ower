import render from './render'
let express = require('express')
import proxy from 'express-http-proxy';
let app = express()
app.use(express.static('public'));
app.use('/api', proxy('https://cheapi.58.com', {
  proxyReqPathResolver(req) {
    return `/api${req.url}`;
  }
}));
app.get('*', function (req, res) {
  render(req,res)
 })
app.listen(3000, function () { 
    console.log('server3000 started')
})
import React from 'react'
import { renderToString } from 'react-dom/server' //把组件转为模板字符串
import {StaticRouter} from 'react-router-dom';//服务端路由用staticRouter
import routes from '../routes'
import Header from '../compontents/Header'
import { getServerStore} from '../store' //服务端仓库
import { Provider } from 'react-redux'
import {
    matchRoutes,
    renderRoutes
} from 'react-router-config';//匹配路由和渲染路由
export default function (req, res) { 
    let context = {csses:[]}
    let store = getServerStore(req);
    //匹配请求路由
    let matchedRoutes = matchRoutes(routes, req.path);
    let promises = [];
    //把所有的请求都放入到数组中，等所有的请求都请求成功后，再渲染dom结构
    matchedRoutes.forEach(item => {
        if (item.route.loadData) { 
            promises.push(new Promise(resolve=>item.route.loadData(store).then(resolve,resolve)))
        }  
    });
    Promise.all(promises).then(() => {
        const html = renderToString(
        <Provider store={store}>
            <StaticRouter context={context} location={req.path}>
                < div className = 'container' >
                    <Header />
                    {
                        renderRoutes(routes)
                    }
                </div>
            </StaticRouter>
        </Provider>
        )
         let cssStr = context.csses.join('\n');
        if (context.action == 'REPLACE') { 
            return res.redirect(302, context.url)
        }else if (context.noFound) { 
            res.statusCode='404'
        }
      res.send(`
        <html>
            <head>
                <title>58服务页</title>
                <style>${cssStr}</style>
            </head>
            <body>
                <div id="root">${html}</div>
                <script>
                    window.content={
                        state:${JSON.stringify(store.getState())}
                    }
                </script>
                <script src='/client.js'></script>
            </body>
        </html>
    `)
    })
}
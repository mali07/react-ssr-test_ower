import axios from 'axios'
export default (req) => axios.create({
    baseURL: 'https://cheapi.58.com',
    cookie:req.get('cookie')||''
})
import Home from './compontents/Home'
import Counter from './compontents/Counter'
import GuessLike from './compontents/GuessLike'
import Person from './compontents/Person'
import NotFound from './compontents/NotFound'
export default [
    {
        path: '/',
        component: Home,
        exact: true,
        key: 'home'
    },
    {
        path: '/counter',
        component: Counter,
        key: '/counter'
    },
    {
        path: '/likes',
        component: GuessLike,
        key: '/likes',
        loadData: GuessLike.loadData
    },
    {
        path: '/person',
        component: Person,
        key: '/person'
    },
    {
        component: NotFound
    }
]